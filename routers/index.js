const express = require('express');
const router = express.Router();
const fs = require('fs');
const nodemailer = require('nodemailer');
const UserContact = require('../model/UserContact');
const ejs = require('ejs');
const path = require('path');
const axios = require('axios');
const cheerio = require('cheerio');
const puppeteer = require('puppeteer');


router.get('/', async (req, res) => {
  return res.render('index' ,{
    title: 'Instagram URL to Video Converter | Instagram Converter',
    message: null
  });
});

router.get('/terms' ,async(req,res) => {
    return res.render('terms',{
        title: 'Terms and Conditions | Instagram Converter'
    });
});

router.get('/privacy' ,async(req,res) => {
    return res.render('privacy-policy',{
      title: 'Privacy And Policies | Instagram Converter'
    });
});

router.get('/about-us', (req,res) => {
  return res.render('AboutUs',{
    title: 'About Us | Instagram Converter'
  });
});
router.get('/contact-us', (req,res) => {
  return res.render('ContactUs',{
    title: 'Contact Us | Instagram Converter',
    message: null
  });
});

// // Post Method for Intagram video converter

// router.post('/convert-video' ,async (req,res) => {
//   try{

//     const { instaUrl } = req.body;
//     const videoData = await getVideoData(instaUrl);
//     const videoPath = await downloadVideoMehtod(videoData.videoUrl);

//     return res.render('videoPage' ,{
//       title: 'Download Video | Instagram Converter',
//       message: `Successfully Converted ,Now Download`,
//       videoTitle: videoData.title,
//       thumbnailUrl: videoData.thumbnailUrl,
//       videoUrl: videoPath
//     });
//   }catch(error){
//     console.log('Error In Convertion Of Url' ,error);
//     return res.render('PageNotFound' ,{
//       title: 'Page Not Found | 404 ',
//       message: `Oops! Error: ${error.message}`
//     });
//   }
// });


// // download routes

// router.get('/download' , async (req, res) => {
//   try{

//     const { instaUrl } = req.query;
//     res.download(instaUrl);

//   }catch(error){
//     console.log('Error In Download Videol' ,error);
//     return res.render('PageNotFound' ,{
//       title: 'Page Not Found | 404 ',
//       message: `Oops! Errro: ${error.message}`
//     });
//   }
// });



// // All function converter and downloader

// async function getVideoData(videoUrl) {
//   const response = await axios.get(videoUrl)
//   const html = response.data
//   console.log('Instagram HTML:', html);
  
//   const regex = /<title>(.*?)<\/title>/
//   const videoTitle = regex.exec(html)[1]
//   const titleMatch = html.match(videoTitle);
//   if (titleMatch) {
//     videoTitle = titleMatch[1];
//   } else {
//     console.log('Error: could not extract video title from HTML');
//     console.log('HTML:', html);
//   }

//   const thumbnailRegex = /"og:image" content="(.*?)"/
//   const thumbnailUrl = thumbnailRegex.exec(html)[1]
//   const thumbnailMatch = html.match(thumbnailRegex);
//   if (thumbnailMatch) {
//     thumbnailUrl = thumbnailMatch[1];
//   } else {
//     console.log('Error: could not extract video thumbnail URL from HTML');
//     console.log('HTML:', html);
//   }

//   const videoRegex = /"og:video" content="(.*?)"/
//   videoUrl = videoRegex.exec(html)[1]
//   const videoMatch = html.match(videoRegex);
//   if (videoMatch) {
//     videoUrl = videoMatch[1];
//   } else {
//     console.log('Error: could not extract video URL from HTML');
//     console.log('HTML:', html);
//   }
  
//   return {
//     videoTitle,
//     thumbnailUrl,
//     videoUrl
//   };
// };

// async function downloadVideoMehtod(videoUrl) {
//   const response = await axios.get(videoUrl, { responseType: 'stream' })
//   const videoFileName = path.basename(videoUrl)
//   const videoFilePath = `public/audio/${videoFileName}`
//   const videoWriteStream = fs.createWriteStream(videoFilePath)

//   response.data.pipe(videoWriteStream)

//   return new Promise((resolve, reject) => {
//     videoWriteStream.on('finish', () => resolve(videoFilePath))
//     videoWriteStream.on('error', reject)
//   });
// };


// Post Method for Intagram video converter
// const ig = require('instagram-scraping');
router.post('/convert-video', async (req,res) => {
  try {

  const url = req.body.url;
  const filePath = await downloadVideo(url);
  const fileName = filePath.substring(filePath.lastIndexOf('/') + 1);
  const title = fileName.substring(0, fileName.lastIndexOf('.'));
  const thumbnailUrl = await getThumbnailUrl(url);

  return res.render('video', {
    videoTitle: title,
    thumbnail: thumbnailUrl,
    fileName: fileName,
    downloadLink: `/download?file=${fileName}`,
    message: 'Successfully converted! Now you can download the video.',
    title: 'Instagram Video Downloader | Instagram Downloader',
  });
  }catch(error){
    console.log('Error In Convertion Of Url' ,error);
    return res.render('PageNotFound' ,{
      title: 'Page Not Found | 404 ',
      message: `Oops! Error: ${error.message}`
    });
  }
});

router.get('/download/:fileName', async (req, res) => {
  try {
    const file = req.query.file;
    const filePath = `/audio/${file}`;
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(`file://${__dirname}/${filePath}, { waitUntil: 'networkidle2' }`);
    await page.click('video');
    await page.waitForTimeout(1000);
    await browser.close();

    const fileStream = fs.createReadStream(filePath);
    res.setHeader('Content-disposition', `attachment; filename=${file}`);
    res.setHeader('Content-type', 'video/mp4');
    fileStream.pipe(res);
  } catch (error) {
    console.log('Error In Convertion Of Url' ,error);
    return res.render('PageNotFound' ,{
      title: 'Page Not Found | 404 ',
      message: `Oops! Error: ${error.message}`
    });
  }
});

const getThumbnailUrl = async (url) => {
  const html = await axios.get(url);
  const thumbnailUrl = html.data.match(/"og:image"\s*content="([^"]+)"/)[1];
  return thumbnailUrl;
};


const downloadVideo = async (url) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: 'networkidle2' });
  await page.waitForSelector('video',{timeout:30000});
  console.log(`Page loaded: ${url}`);
  const videoUrl = await page.evaluate(() => {
    const videoElement = document.querySelector('video');
    if (videoElement) {
      return videoElement.src;
    } else {
      return null;
    }
  });
  console.log(`Video URL found: ${videoUrl}`);
  await browser.close();

  if (videoUrl) {
    const response = await axios.get(videoUrl, { responseType: 'stream' });
    const fileName = videoUrl.substring(videoUrl.lastIndexOf('/') + 1);
    const filePath = `/audio/${fileName}`;
    const writeStream = fs.createWriteStream(filePath);
    response.data.pipe(writeStream);
    return new Promise((resolve, reject) => {
      writeStream.on('finish', () => {
        resolve(filePath);
      });
      writeStream.on('error', reject);
    });
  } else {
    console.log('Video URL not found in HTML')
    throw new Error('Video URL not found in HTML');
  }
};


// const downloadVideo = async (url) => {
//   const browser = await puppeteer.launch();
//   const page = await browser.newPage();
//   await page.goto(url, { waitUntil: 'networkidle2' });
//   await page.waitForSelector('video');
//   const videoUrl = await page.evaluate(() => {
//     const videoElement = document.querySelector('video');
//     if (videoElement) {
//       return videoElement.src;
//     } else {
//       return null;
//     }
//   });
//   await browser.close();

//   if (videoUrl) {
//     const response = await axios.get(videoUrl, { responseType: 'stream' });
//     const fileName = videoUrl.substring(videoUrl.lastIndexOf('/') + 1);
//     const filePath = `/audio/${fileName}`;
//     const writeStream = fs.createWriteStream(filePath);
//     response.data.pipe(writeStream);
//     return new Promise((resolve, reject) => {
//       writeStream.on('finish', () => {
//         resolve(filePath);
//       });
//       writeStream.on('error', reject);
//     });
//   } else {
//     console.log('Video URL not found in HTML')
//     throw new Error('Video URL not found in HTML');
//   }
// };



// email template load from viewfiles for success message
const successEmail = fs.readFileSync('./project_views/ContactSuccess/SuccessEmail.ejs','utf-8');

// Transporter
const transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT, 
  auth: {
    user:process.env.SMTP_USER_EMAIL,
    pass:process.env.SMTP_USER_PASSWORD
  },
  secure: process.env.SECURE_SMTP,
});

// form submition 

router.post('/send-contact-form' ,async (req , res) => {
  try{

    const { name , email , message } = req.body;
    const isMessage = await UserContact.findOne({message});
    if(isMessage){
      return res.render('ContactUs', {
        title: 'Contact Us | Instagram Converter',
        message: 'This Message Already Sent ,Send New Message Please.'
      });
    }
    const newMessage = await new UserContact({
      name,
      email,
      message
    });
    await newMessage.save();
    const renderedTemplate = ejs.render(successEmail,{ newMessage });
    // Construct the password reset email
    const mailOptions = {
      from: process.env.SMTP_FROM_EMAIL ,
      to: newMessage.email,
      subject: `Contact Submitted Successfull`,
      html: renderedTemplate,
    };
    // Send the email
    transporter.sendMail(mailOptions, (error) => {
      if (error) {
        console.log('An error occurred in Transporter: ' + error.message);
        return res.render('PageNotFound' ,{
          title: 'Page Not Found | Instagram Converter ',
          message: `Oops! Error:${error.message}` 
        });
      } else {
        return res.render('ContactUs', {
          title: 'Contact Us | Instagram Converter ',
          message: 'Successfully Submitted Contact Form.'
        });
      }
    });
  }catch(error){
    console.log('An error occurred: ' + error.message);
    return res.render('PageNotFound' ,{
      title: 'Page Not Found | Instagram Converter ',
      message: `Oops! Error:${error.message}` 
    });
  }
});


module.exports = router;

