# Instagram Video Downloader

Instagram Video Downloader is a Node.js web application that allows users to download Instagram videos by entering the video URL. The application is built using `Node.js, Express, Axios, EJS, and Bulma CSS.`

## Installation

To install the application, you need to have Node.js installed on your computer. You can download and install Node.js from the official website: `https://nodejs.org/en/.`

Once you have Node.js installed, you can clone the repository to your local machine:


1. Clone the repository from github `https://github.com/Saurabhdixit93/instagram-video-downloader.git`After cloning the repository, navigate to the project directory and install the dependencies:
2. Install dependencies using `npm install`
3. set `.env` file's variables according to your crediantials



## Usage

To start the application, run the following command in the project directory:

1. Start the server using `npm start`


This will start the application on http://localhost:5000/. You can then access the application by opening a web browser and navigating to http://localhost:5000/.

## Features

The application has the following features:

- Enter an Instagram video URL to download the video
- Display the video thumbnail and title
- Download the video in MP4 format
- Terms and conditions page
- Privacy policy page
- About page with information about the technologies used in the project

## Technologies

The application is built using the following technologies:

<div align="center">
  <img src="https://img.shields.io/badge/-Node.js-339933?logo=node.js&logoColor=white&style=flat-square" alt="Node.js">
  <img src="https://img.shields.io/badge/-Express.js-000000?logo=express&logoColor=white&style=flat-square" alt="Express.js">
  <img src="https://img.shields.io/badge/-Nodemailer-4ABBF2?logo=nodemailer&logoColor=white&style=flat-square" alt="Nodemailer">
  <img src="https://img.shields.io/badge/-EJS-8D1F68?logo=ejs&logoColor=white&style=flat-square" alt="EJS">
  <img src="https://avatars.githubusercontent.com/u/7857356?s=200&v=4" 
  alt="Axios">
  <img src="https://bulma.io/images/bulma-logo.png" 
  alt="Bulma Css">
</div>

- Node.js
- Express
- Axios
- EJS
- Bulma CSS

## License

This project is licensed under the MIT License - see the LICENSE file for details.

## Contributing

Contributions are welcome! To contribute to the project, please follow these steps:

1. Fork the repository
2. Create a new branch (git checkout -b feature/your-feature-name)
3. Make your changes
4. Commit your changes (git commit -am 'Add some feature')
5. Push to the branch (git push origin feature/your-feature-name)
6. Create a new pull request

## Support

If you encounter any issues or have any questions, please feel free to create an issue on the repository or contact the project maintainer.

## Acknowledgements

- [Bulma CSS](https://bulma.io/) - used for styling the application


## Project Structure


├── node_modules/
├── public/
│   ├── All files with images
├── project_views/
|   ├── ContactSuccess/
|   |    ├── SuccessEmail.ejs
│   ├── _footer.ejs
│   ├── _header.ejs
│   ├── PageNotFound.ejs
│   ├── _header.ejs
│   ├── AboutUs.ejs
│   ├── ContactUs.ejs
│   ├── privacy-policy.ejs
│   ├── terms.ejs
│   └── videoPage.ejs
├── .env.example
├── .gitignore
├── index.js
├── LICENCE
├── README.md
├── package-lock.json
└── package.json


## Screenshots

![Home Page](screenshots/home.png)

![Video Page](screenshots/video.png)

![Terms and Conditions Page](screenshots/terms.png)

![Privacy Policy Page](screenshots/privacy.png)

![About Page](screenshots/about.png)

## Conclusion

That's it! You have successfully created an Instagram Video Downloader web application using Node.js, Express, Axios, EJS, and Bulma CSS. The application allows users to download Instagram videos by entering the video URL and displays the video thumbnail and title. It also includes terms and conditions, privacy policy, and about pages.

## Devloped By -
 
<b>Saurabh Dixit<b>

